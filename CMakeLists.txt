cmake_minimum_required(VERSION 3.6)
include(GenerateExportHeader)

project(xvcd_jlink)

set(SOURCE_FILES
        JlinkUtil.c
        io_jlink.c
        xvcd.c
        getopt.c)

add_executable(xvcd_jlink ${SOURCE_FILES})

set(CMAKE_FIND_LIBRARY_SUFFIXES ".lib" ".dll" ".so" ".dylib")

find_library(JLINK_ARM_PATH
        NAMES JLinkARM jlinkarm libjlinkarm
        HINTS /opt/SEGGER/JLink /Applications/SEGGER/JLink ${CMAKE_CURRENT_LIST_DIR}/lib)
message("Found lib JLinkARM ${JLINK_ARM_PATH}")

set_target_properties(xvcd_jlink PROPERTIES ENABLE_EXPORTS true)
target_link_libraries(xvcd_jlink ${JLINK_ARM_PATH} ${XCURSOR_LIB})