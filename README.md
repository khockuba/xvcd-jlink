# XVCD JLink

Xilinx Virtual Cable using JLink Debugger. Originally based on https://github.com/fantomgs/xvcd-jlink

Xilinx Virtual Cable (XVC) is a TCP/IP-based protocol that acts like a JTAG cable and provides a means to access and debug your FPGA or SoC design without using a physical cable. 

**This capability helps facilitate hardware debug for designs that:**

- Have the FPGA in a hard-to-access location, where a "lab-PC" is not close by
- Do not have direct access to the FPGA pins – e.g. the JTAG pins are only accessible via a local processor interface
- Need to efficiently debug Xilinx FPGA or SoC systems deployed in the field to save on costly or impractical travel and and reduce the time it takes to debug a remotely located system

**Key Features and Benefits**

- Ability to debug a system over an internal network, or even the internet
- Debug via Vivado Logic Analyzer IDE exactly as if directly connected to design via standard JTAG or parallel cable
- Zynq®-7000 demonstration with Application Note and Reference Designs available in [XAPP1251](https://www.xilinx.com/content/dam/xilinx/support/documentation/application_notes/xapp1251-xvc-zynq-petalinux.pdf) - Xilinx Virtual Cable Running on Zynq-7000 Using the PetaLinux Tools
- Extensible to allow for safe, secure connections
- XVC protocol specification and example designs available on [Github](https://github.com/Xilinx/XilinxVirtualCable)
